import { either, function as func } from 'fp-ts';
import fs from 'fs';

export const readFileSync = (
    filePath: string,
    encoding: BufferEncoding = 'utf8'
): either.Either<Error, string> =>
    either.tryCatch(() => fs.readFileSync(filePath, encoding), either.toError);

export const existsSync = (filePath: string): boolean =>
    func.pipe(
        either.tryCatch(() => fs.existsSync(filePath), either.toError),
        either.fold(() => false, func.identity)
    );

export const writeFileSync = (
    filePath: string,
    content: string
): either.Either<Error, void> =>
    either.tryCatch(() => fs.writeFileSync(filePath, content), either.toError);

export const readdirSync = (
    dirPath: string
): either.Either<Error, ReadonlyArray<string>> =>
    either.tryCatch(() => fs.readdirSync(dirPath), either.toError);

type MkdirSyncType = Parameters<typeof fs.mkdirSync>;
export const mkdirSync = (
    dirPath: string,
    options?: MkdirSyncType[1]
): either.Either<Error, void> =>
    func.pipe(
        either.tryCatch(() => fs.mkdirSync(dirPath, options), either.toError),
        either.map(() => func.constVoid())
    );

type CpSyncType = Parameters<typeof fs.cpSync>;
export const cpSync = (
    srcPath: string,
    destPath: string,
    options?: CpSyncType[2]
): either.Either<Error, void> =>
    either.tryCatch(
        () => fs.cpSync(srcPath, destPath, options),
        either.toError
    );
