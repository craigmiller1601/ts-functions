import * as Try from './Try';
import { pipe } from 'fp-ts/function';
import * as Option from 'fp-ts/Option';

export const parseE = <T>(json: string): Try.Try<T> =>
    Try.tryCatch(() => JSON.parse(json) as T);

export const stringifyE = (value: unknown): Try.Try<string> =>
    stringifyIndentE(0)(value);

export const stringifyIndentE =
    (indent: number) =>
    (value: unknown): Try.Try<string> =>
        Try.tryCatch(() => JSON.stringify(value, null, indent));

export const parseO = <T>(json: string): Option.Option<T> =>
    pipe(parseE<T>(json), Option.fromEither);

export const stringifyO = (value: unknown): Option.Option<string> =>
    pipe(stringifyE(value), Option.fromEither);

export const stringifyIndentO =
    (indent: number) =>
    (value: unknown): Option.Option<string> =>
        pipe(stringifyIndentE(indent)(value), Option.fromEither);
