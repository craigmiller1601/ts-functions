import * as TaskEither from 'fp-ts/TaskEither';
import * as Task from 'fp-ts/Task';
import { Lazy } from 'fp-ts/function';
import * as TaskEitherExt from './TaskEitherExt';
import * as Either from 'fp-ts/Either';

export type TaskTry<T> = TaskEither.TaskEither<Error, T>;

export const tryCatch = <T>(fn: Lazy<Promise<T>>): TaskTry<T> =>
    TaskEither.tryCatch(fn, Either.toError);

export const getOrThrow = <T>(theTry: TaskTry<T>): Task.Task<T> =>
    TaskEither.fold((ex) => {
        throw ex;
    }, Task.of)(theTry);

export const chainTryCatch =
    <T1, T2>(fn: (value: T1) => Promise<T2>) =>
    (ma: TaskTry<T1>): TaskTry<T2> =>
        TaskEitherExt.chainTryCatch(fn, Either.toError)(ma);
