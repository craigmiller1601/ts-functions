import * as Either from 'fp-ts/Either';

export const chainTryCatch =
    <E, T1, T2>(fn: (value: T1) => T2, onRejected: (reason: unknown) => E) =>
    (ma: Either.Either<E, T1>): Either.Either<E, T2> =>
        Either.chain((value: T1) =>
            Either.tryCatch(() => fn(value), onRejected)
        )(ma);
