import { describe, it, expect } from 'vitest';
import * as TaskEitherExt from '../src/TaskEitherExt';
import * as TaskEither from 'fp-ts/TaskEither';
import * as Either from 'fp-ts/Either';

const te: TaskEither.TaskEither<Error, string> = TaskEither.right('Hello');

describe('TaskEitherExt', () => {
    describe('chainTryCatch', () => {
        it('successful promise', async () => {
            const result = await TaskEitherExt.chainTryCatch(
                // eslint-disable-next-line @typescript-eslint/require-await
                async (value: string) => `${value} World`,
                Either.toError
            )(te)();
            expect(result).toEqualRight('Hello World');
        });

        it('failed promise', async () => {
            // eslint-disable-next-line @typescript-eslint/require-await
            const result = await TaskEitherExt.chainTryCatch(async () => {
                throw new Error('Dying');
            }, Either.toError)(te)();
            expect(result).toEqualLeft(new Error('Dying'));
        });
    });
});
