import { describe, it, expect } from 'vitest';
import * as Either from 'fp-ts/Either';
import * as EitherExt from '../src/EitherExt';

const e: Either.Either<Error, string> = Either.right('Hello');

describe('EitherExt', () => {
    describe('chainTryCatch', () => {
        it('successful', () => {
            const result = EitherExt.chainTryCatch(
                (value: string) => `${value} World`,
                Either.toError
            )(e);
            expect(result).toEqualRight('Hello World');
        });

        it('fails', () => {
            const result = EitherExt.chainTryCatch(() => {
                throw new Error('Dying');
            }, Either.toError)(e);
            expect(result).toEqualLeft(new Error('Dying'));
        });
    });
});
